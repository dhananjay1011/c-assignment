#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct as9
{
    int id,quantity;
    char name[50];
    double price;
}item_t;

typedef enum{EXIT, ADD, FIND, DISPLAY_ALL, EDIT, DELETE}choice;

#define ITEMDB "item.db"
#define ITEMDB_TEMP "temp.db"
// #define COPYITEMDB "itemcopy.db"

void add();
void accept(item_t * it);
void display(item_t * it);
int get_next_id();
void display_all();
void find();
void edit();
void delete();

int main(){
    int ch;
    do
    {
        printf("\n0.Exit  1.ADD 2.FIND 3.DISPLAY_ALL 4.EDIT 5.DELETE \nEnter your choice: ");
        scanf("%d",&ch);
        
        switch (ch)
        {
           case EXIT:
           break;

           case ADD:
           add();
           break;

           case FIND:
           find();
           break;

           case DISPLAY_ALL:
           display_all();
           break;

           case EDIT:
           edit();
           break;

           case DELETE:
           delete();
           break;

        default:
        printf("\nInvalid choice");
            break;
        }
    } while (ch!=0);
    return 0;
}

void add(){
    item_t it;
    FILE *fp;
    long size=sizeof(item_t);
    fp=fopen(ITEMDB,"ab");
    if(fp==NULL){
        perror("\nFailed to open database");
        return;
    }
    accept(&it);
    fwrite(&it,size,1,fp);
    display(&it);
    printf("\n");
    fclose(fp);
    return;
}

void accept(item_t * it){
    printf("\nEnter the name of item: ");
    fflush(stdin);
    gets(it->name);
    printf("\nEnter the price of item: ");
    fflush(stdin);
    scanf("%lf",&it->price);
    printf("\nEnter the quantity of item: ");
    fflush(stdin);
    scanf("%d",&it->quantity);
    it->id=get_next_id();
    return;
}

void display(item_t * it){
    printf("\nItem id: %d,  Item: %s,  Item price: %.2lf,  Item quantity: %d",it->id,it->name,it->price,it->quantity);
    return;
}

int get_next_id(){
    FILE *fp;
    item_t it;
    int count=0;
    long size=sizeof(item_t);
    fp=fopen(ITEMDB,"rb");
    if(fp==NULL){
        perror("\nFailed to open database");
        return (count+1);
    }
    fseek(fp,-size,SEEK_END);
    if(fread(&it,size,1,fp)>0)
        count=it.id;
    fclose(fp);
    // printf("\nHere\n");
    return (count+1);  
}

void display_all(){
    FILE *fp;
    item_t it;
    // int count=0;
    long size=sizeof(item_t);
    fp=fopen(ITEMDB,"rb");
    if(fp==NULL){
        perror("\nFailed to open database");
        return;
    }
    while(fread(&it,size,1,fp)>0)
     display(&it);
    fclose(fp);
    printf("\n");
    return;
}

void find(){
    char name[50];
    printf("\nEnter the product to be searched: ");
    fflush(stdin);
    gets(name);
    FILE *fp;
    item_t it;
    // int count=0;
    long size=sizeof(item_t);
    fp=fopen(ITEMDB,"rb");
    if(fp==NULL){
        perror("\nFailed to open database");
        return;
    }
    while(fread(&it,size,1,fp)>0){
     if(strstr(it.name,name)!=NULL)
     display(&it);
     }
    fclose(fp);
    printf("\n");
    return;   
}

void edit(){
    item_t it;
    int id,found=0;
    printf("\nEnter the id of product to be edited: ");
    scanf("%d",&id);
    FILE *fp;
    long size=sizeof(item_t);
    fp=fopen(ITEMDB,"rb+");
    if(fp==NULL){
        perror("\nFailed to open database");
        return;
    }
    while(fread(&it,size,1,fp)>0){
     if(it.id==id){
     found=1;
     break;
     }
    }
	if(found) {
		item_t nit;
		accept(&nit);
		nit.id = it.id;
		fseek(fp, -size, SEEK_CUR);
		fwrite(&nit, size, 1, fp);
		printf("Item updated.\n");
	}
	else 
		printf("Item not found.\n");
	fclose(fp);
}
void delete() {
	FILE *fp;
	FILE *fp_tmp;
	int found=0,id;
    item_t nit,it;
    long size=sizeof(item_t);
    printf("Enter the id of the record to be deleted: ");
    scanf("%d",&id);

	fp=fopen(ITEMDB, "rb");
    if(fp==NULL){
        perror("\nFailed to open database");
        return;
    }

    fp_tmp=fopen(ITEMDB_TEMP,"wb");
    if(fp_tmp==NULL){
        perror("\nFailed to open database");
        return;
    }

    while (fread(&nit,size,1,fp)>0) {
		if (nit.id==id) {
			printf("A record with requested name found and deleted.\n");
			found=1;
		} 
        else 
			fwrite(&nit,size,1,fp_tmp);
	}

	if (! found) {
		printf("\nWrong Id\n");
	}

	fclose(fp);
	fclose(fp_tmp);

	remove(ITEMDB);
	rename(ITEMDB_TEMP, ITEMDB);

	return; 
}