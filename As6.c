#include<stdio.h>
#include<stdlib.h>
typedef struct Student{
	int roll;
	char name[100];
	char std[10];
	char subject[6][100];
	int marks[6];
}student;
int count = 0;
int front = -1;
int rear = -1;
int isFull(int size){
	return count == size;
}
int isEmpty(){
	return count==0;
}
void push(student *arr,int size){   //add
		student s;
		printf("Enter the details of student:\n");
		printf("Roll: ");
		scanf("%d",&s.roll);
		printf("Name: ");
		scanf("%s",s.name);
		printf("Std: ");
		scanf("%s",s.std);
		for(int i=0;i<6;i++){
			printf("Subject and marks: ");
			scanf("%s %d",s.subject[i],&s.marks[i]);
		}
		if(front == -1 && rear == -1){
			front++;
			rear++;
		}
		else
			rear=(rear+1)%size;
		arr[rear]=s;
		count++;
		printf("Student details added successfully!!!\n");
}
void pop(student* arr,int size){
		student s = arr[front];
		front++;
		if(front == size)
			front = 0;
		count--;
		printf("Student deleted sucessfully with roll no: %d\n",s.roll);
}
void display(student* arr,int size){
	printf("Data in the queue:\n");
	int j=front;
	for(int i=0;i<count;i++){
		student s = arr[j];
		printf("Student details:\n");
		printf("Roll: %d\n",s.roll);
		printf("Name: %s\n",s.name);
		printf("Std: %s\n",s.std);
		for(int k=0;k<6;k++){
			printf("Subject: %s and Marks: %d\n",s.subject[k],s.marks[k]);
		}
		j++;
		if(j == size)
			j=0;
		printf("---------------------------\n");
	}
}
int main(){
	int size,choice;
	printf("Enter the size of circular queue: ");
	scanf("%d",&size);
	student *arr = (student*)malloc(size*sizeof(student));
	while(1){
		printf("1. Push\n2. Pop\n3. Display\n4. Exit\n");
		printf("Enter the choice:");
		scanf("%d",&choice);
		switch(choice){
			case 1:
				if(isFull(size))
					printf("Queue is full!!!\n");
				else
					push(arr,size);
				break;
			case 2:
				if(isEmpty())
					printf("Queue is empty!!!\n");
				else
					pop(arr,size);
				break;
			case 3:
				if(isEmpty())
					printf("Queue is empty!!!\n");
				else
					display(arr,size);
				break;
			case 4:
				free(arr);
				exit(0);
			default:
				printf("Enter the valid choice\n");
		}
	}
}
