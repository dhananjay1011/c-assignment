#include<stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
enum operations{
	EXIT,ADDFIRST,ADDLAST,DISPLAYFORWARD,DISPLAYBACKWARD,DELETEALL,FINDBYID,FINDBYNAME
};
typedef struct accountHolder
{
	char name[20];
	char address[50];
	char mobNo[10];
}AccountHolder;

typedef struct account{
	int id;
	char type[20];
	float balance;
	AccountHolder ah;
}Account;

struct node{
	Account acc;
	struct node* prev;
	struct node* next;
};
typedef struct node Node;
Node *head=NULL;

Node* createNode(){
	Node *node=(Node*)malloc(sizeof(Node));
	if(node==NULL){
		printf("malloc() failed.\n");
        exit(1);
    }
	else
	{
	node->next=NULL;
	node->prev=NULL;
	printf("Enter Account ID:");
	scanf("%d",&node->acc.id);
	printf("Enter Amount to be Deposited:");
	scanf("%f",&node->acc.balance);
	printf("Enter Account type:");
	scanf("%s",node->acc.type);
	getchar();
	printf("Enter Account Holder's Name:");
	scanf("%s",node->acc.ah.name);
	printf("Enter Account Holder's Address:");
	scanf("%s",node->acc.ah.address);
	printf("Enter Account Holder's Mobile No.:");
	scanf("%s",node->acc.ah.mobNo);
	}
	return node;
}
void addFirst(){
	Node* newNode=createNode();
	if(head==NULL)										
		head=newNode;
	else
	{
		newNode->next=head;
		head->prev=newNode;
		head=newNode;
	}
    printf("Account added successfully");
}
void addLast(Node* newNode){

	if(head==NULL)										
			head=newNode;
	else
	{
		Node* trav=head;
		while(trav->next!=NULL)//traverse till last node
			trav=trav->next;

		newNode->prev=trav;
		trav->next=newNode;
	}
}

void displayAccountDetails(Node *node)
{
	printf("\nAccount ID: %d\n",node->acc.id);
	printf("Account Type: %s\n",node->acc.type);
	printf("Account Balance: %f\n",node->acc.balance);
	printf("Account Holder's Name: %s\n",node->acc.ah.name);
	printf("Account Holder's Address: %s\n",node->acc.ah.address);
	printf("Account Holder's Mobile Number: %s\n",node->acc.ah.mobNo);
	printf("-----------------------------------------------\n");
}
void displayForward()
{
	Node *trav=head;
	while(trav!=NULL)
	{
		//Print account details by calling function
		displayAccountDetails(trav);
		trav=trav->next;
	}
}
void displayBackward()
{
	Node *trav=head;
	while(trav->next!=NULL)
		trav=trav->next;
	while(trav!=NULL)
	{
		//Print account details by calling function
		displayAccountDetails(trav);
		trav=trav->prev;
	}

}
void deleteAll()
{
	Node* current,*next;
	current=head;

	while(current!=NULL)
	{
		next=current->next;
		free(current);
		current=next;
	}
	head=NULL;
}
Node* findById(int id)
{
	Node *trav=head;
	while(trav!=NULL)
	{
		 if(trav->acc.id==id)
			 return trav;
		 trav=trav->next;
	}
	return NULL;
}
Node* findByName(char *name)
{
	Node *trav=head;
	while(trav!=NULL)
	{
		 if(!stricmp(trav->acc.ah.name,name))
			 return trav;
		 trav=trav->next;
	}
	return NULL;
}

int main()
{
	setvbuf(stdout,NULL,_IONBF,0);
	int id;
	char name[20];
	Node* newNode=(Node*)malloc(sizeof(Node));
	Node* temp=NULL;
	int choice;
	do{
		printf("0. Exit\n1. AddFirst\n2. AddLast\n3. Display Forward\n4. Display Backward\n5. DeleteAll\n6. Find by id\n7. Find by Name\n");
		printf("Enter choice:");
		scanf("%d",&choice);
		switch(choice)
		{
		case ADDFIRST:
			addFirst();
			break;
		case ADDLAST:
			newNode=createNode(newNode);
			addLast(newNode);
			break;
		case DISPLAYFORWARD:
			displayForward(head);
			break;
		case DISPLAYBACKWARD:
			displayBackward();
			break;
		case DELETEALL:
			deleteAll();
			break;
		case FINDBYID:
			printf("Enter AccountID:");
			scanf("%d",&id);
			temp=findById(id);
			if(temp!=NULL)
			{
				printf("Account Details are:\n");
				displayAccountDetails(temp);
			}
			else
				printf("Account doesnot exist with Id %d.\n",id);
			break;
		case FINDBYNAME:
			printf("Enter Account Holder's Name:");
			scanf("%s",name);
			temp=findByName(name);
			if(temp!=NULL)
			{
				printf("Account Details are:\n");
				displayAccountDetails(temp);
			}
			else
				printf("Account doesnot exist with name %s.\n",name);
			break;
		default: 
		    printf("Invalid Choice!");
        break;
		}
	}while(choice!=0);
	return 0;
}
