#include<stdio.h>
#include<stdlib.h>
typedef enum{EXIT,ADD,DELETE,FIND_MAX,FIND_MIN,SUM} ch;
int main()
{
    double ar[10]={0,0,0,0,0,0,0,0,0,0};
    ch choice;
    void add(double a[]);
    int check_full(double arr[]);
    void show (double ar[]);  
    void del(double a[]);
    void sum(double a[]);
    void find_min(double a[]);
    void find_max(double a[]);

    do
    {
    printf("\nEnter the choice:0.EXIT 1.ADD 2.DELETE 3.FIND_MAX 4.FIND_MIN 5.SUM\t");
    scanf("%d",&choice);
    switch(choice)
    {
        case ADD:
        add(ar);
        int a=0;
        a=check_full(ar);

        if(a==1)
        printf("\nList is Full\n");
        show(ar);
        break;

        case DELETE:
        del(ar);
        show(ar);
        break;
       
        case FIND_MAX:
        find_max(ar);
        show(ar);
        break;
       
        case FIND_MIN:
        find_min(ar);
        show(ar);
        break;
   
        case EXIT:
        break;
         
        case SUM:
        sum(ar);
        show(ar);
        break;

        default:
        printf("Choice not available");
        break;
    }
}while (choice!=EXIT);
return 0;

}
void add(double ars[])
{ 
    int index,i;
    double num;
    printf("\nAvailable indexes are");
    for(i=0;i<10;i++)
    {
          if(ars[i]==0)
          printf("%8d",i+1);
          else
          continue;
         
    };

    printf("\nEnter the number and index\t");
    scanf("%lf%d",&num,&index);

    if(ars[index-1]==0)
    ars[index-1]=num;
    else
     printf("\nNot available\n");
}

int check_full(double arr[])
{
       int i,s;
       for(i=0,s=0;i<10;i++)
   {
        if(arr[i]!=0)
        s++;
       else 
      continue;
    };
       if(s==10)
        return 1;
     else 
      return 0;
}

void del(double ars[])
{ 
    int index,i;
    double num;
    printf("\nAvailable indices to delete with number");
    for(i=0;i<10;i++)
    {
          if(ars[i]!=0)
          printf("   %d %lf\t",i+1,ars[i]);
          else
        continue;
    };

    printf("\nEnter the index\t");
    scanf("%d",&index);

    if(index<11 && ars[index-1]!=0)
    ars[index-1]=0;
    else
    printf("\nNot available to delete\n");
}

void find_max(double ars[])
{ 
      int i,max_ind=0;
      for(i=0;i<9;++i)
     {
       if(ars[i+1]>ars[max_ind])
       {
           max_ind=i+1;
       }
       else
       continue;
       
     }   
    
    printf("\nMax of these is %lf and index is %d\n",ars[max_ind],max_ind+1);
}

void find_min(double ars[])
{ 
      int i,min_ind=0;
    for(i=0;i<9;i++)
    {
        if(ars[i+1]<ars[min_ind])
       {
           min_ind=i+1;
       }
       else
       continue;
    }
    printf("\nMin of these is %lf and index is %d\n",ars[min_ind],min_ind+1);
}

void sum(double ars[])
{ 
    int i;
    double sum=0;
    for(i=0;i<10;i++)
    {
        sum=sum+ars[i];
    };
    printf("\nThe sum is %lf",sum);
}

void show (double ar[])
{
    int i;
    printf("\n");
    for(i=0;i<10;i++)
    printf("%lf   ",ar[i]);
    printf("\n");
}