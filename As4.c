#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define SIZE 10
typedef struct 
{
    int bookid;
    char name[20];
    double price;
}book;

void enter (book b[]);
void show(book b[]);
void merge(book b[],int ,int);
void mergesort(book b[],int ,int,int);
int compare(const void *a,const void *b);

int main()
{
    book b[SIZE];
    enter(b);
    show(b);
    merge(b, 0, SIZE-1);
    printf("\n\nArranging price in descending order");
    show(b);
    printf("\n\nArranging names in ascending order");
    qsort(b,SIZE,sizeof(book),compare);
    show(b);

}

void show(book b[])
{
    int i;
    printf("\n");
    for(i=0;i<SIZE;i++)
    printf("\nBook ID. %d, \tBook Name- %s,  \tBook Price-%.2lf ",b[i].bookid,b[i].name,b[i].price);
}

void enter (book b[])
{
    int i;
    for(i=0;i<SIZE;i++)
   { printf("\nEnter Book ID.\t");
    scanf("%d",&b[i].bookid);
    fflush(stdin);
    printf("Enter Book Name- \t");
    gets(b[i].name);
    fflush(stdin);
    printf("Enter Book Price-\t");
    scanf("%lf",&b[i].price);
   fflush(stdin);
   }
}

void merge(book b[],int l ,int r)
{
  int mid;
  if(l<r)
  {
      mid=(l+r)/2;
      merge(b,l,mid);
      merge(b,mid+1,r);
      mergesort(b,l,mid,r);
  }
}
void mergesort(book b[],int l ,int m,int r)
{
    int i,j,k;
    book c[SIZE];
    i=l;  j=m+1; k=l;
    while(i<=m&&j<=r)
    {
        if(b[i].price>=b[j].price)
      {
            c[k].price=b[i].price;
            i++;  
      }
      else
      {
          c[k].price=b[j].price;
          j++;
      }
      k++;
    }
     if (j>r)
     {
         while(i<=m)
        {
            c[k].price=b[i].price;
            i++;  
            k++;
        }
     }
        else if(i>m)
        {
            while(j<=r)
          {  c[k].price=b[j].price;
            j++;
            k++;
          } 
       }
for(k=l;k<=r;k++)
b[k].price=c[k].price;
}

int compare(const void *a,const void *b)
{
  book *e=(book*)a;
  book *f=(book*)b;
  return (strcmp(e->name,f->name));
}
