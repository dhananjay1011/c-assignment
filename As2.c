#include<stdio.h>
#include<stdlib.h>
#include<string.h>
typedef enum{Exit,str_cpy, str_cat, str_cmp, str_rev}ch;
void strcopy (char *dest, char *src);
void strcatt(char *dest, char *src);
int str_comp(char *s1, char *s2);
int string_length(char*);
void reverse(char*);

int main()
{
    ch choice;
    char src[30],dest[30];
    char s1[30],s2[30];
    char s[30];

    do
    {    printf("\nEnter your choice 0.Exit 1.str_cpy 2.str_cat 3.str_cmp 4.str_rev\t");
        
         scanf("%d",&choice);
         switch (choice)
         {
            case Exit:
              break;

            case str_cpy:
                printf("\nEnter the source string:   ");
                fflush(stdin);
                gets(src);
                printf("\nEnter the destination string:   ");
                fflush(stdin);
                gets(dest);
                strcopy(dest,src);
                printf("\n Source string is %s",src);
                fflush(stdin);
                printf("\n Destination string is %s",dest);
                fflush(stdin);
                break;

            case str_cat:
                printf("\nEnter the source string:   ");
                fflush(stdin);
                gets(src);
                printf("\nEnter the destination string:   ");
                fflush(stdin);
                gets(dest);
                strcatt(dest,src);
                printf("\n Source string is %s",src);
                fflush(stdin);
                printf("\n Destination string is %s",dest);
                fflush(stdin);
              break;

            case str_cmp:
            printf("\nEnter the first string:   ");
            fflush(stdin);
            gets(s1);
            printf("\nEnter the second string:   ");
            fflush(stdin);
            gets(s2);
            if(str_comp(s1,s2)==0)
          {
            printf("\nStrings are identical ");
          }
            else
          {
            printf("\nEntered strings are not same!");
          }
          break;

            case str_rev:
             printf("\nEnter a string :\t");
             fflush(stdin);
             gets(s);
             
             reverse(s);
             printf("Reverse of the string is \"%s\"\n",s);
              fflush(stdin);
              break;
        
            default:
              printf("Choice is not available");
              break;
         }

    } while (choice!=0);
    
}

void strcopy (char *dest, char *src)
{
    while(*src != '\0')
    {
        *dest = *src;
        dest++;
        src++;
    }
    *dest = '\0';
}

void strcatt(char *dest, char *src)
{
    while(*dest != '\0')
    {
        dest++;
    }

    while(*src != '\0')
    {
        *dest = *src;
        dest++;
        src++;
    }

    *dest = '\0';
}

int str_comp(char *s1, char *s2)
{
    while( ( *s1 != '\0' && *s2 != '\0' ) && *s1 == *s2 )
    {
        s1++;
        s2++;
    }

    if(*s1 == *s2)
    {
        return 0; // strings are identical
    }

    else
    {
        return *s1 - *s2;
    }
}

void reverse(char *s)
{
   int length, c;
   char *start, *end, temp;
 
   length = string_length(s);
   start  = s;
   end    = s;
 
   for (c = 0; c < length - 1; c++)
      end++;
 
   for (c = 0; c < length/2; c++)
   {        
      temp   = *end;
      *end   = *start;
      *start = temp;
 
      start++;
      end--;
   }
}
 
int string_length(char *pointer)
{
   int c = 0;
 
   while( *(pointer + c) != '\0' )
      c++;
 
   return c;
}